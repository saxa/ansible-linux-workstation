# ansible-fedora-workstation

This playbook bootstraps and manages user-critical apps and applications
on a personal workstation

## Usage

`ansible-playbook -i inventory --diff -K playbook.yml -e personal_fedora=true --vault-password-file=<your_password_file>`

Options for -e are:
- personal_fedora=true
- work_fedora=true
- personal_ubuntu=true
- work_ubuntu=true

Where <the_targeted_pc> is the configuration file you have made for yourself (template available at machine_configurations/000-template.yml

Please also review the options in the vars/ folder.

If you are using my roles from the [ansible-workstation group](https://gitlab.com/ansible-opletal/ansible-workstation), you can include your app config files under config_files/<directory_of_app>

## CI Tested
- Debian Testing
- Ubuntu 20.04
- Fedora 32
- Fedora 33
- Alpine 3.12
- Alpine Edge

## Not tested in CI, but should work without a problem
- PopOS 20.04
- PopOS 20.10
- Ubuntu 20.10
